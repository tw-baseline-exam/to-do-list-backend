package com.tw.todoitems.model;

public enum ItemStatus {
    ACTIVE, COMPLETED;
}
